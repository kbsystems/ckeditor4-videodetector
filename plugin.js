/*
*
*   Plugin developed by Dimitri Conejo
*   www.dimitriconejo.com
*
*/

CKEDITOR.plugins.add( 'videodetector', {
    icons: 'videodetector',
    init: function( editor ) {

        var pluginDirectory = this.path;
        editor.addContentsCss(pluginDirectory + 'videodetector.css');

        editor.addCommand('videodetector', new CKEDITOR.dialogCommand('videoDialog'));
        editor.ui.addButton( 'VideoDetector', {
            label: 'Insert a Youtube, Vimeo or Dailymotion video',
            command: 'videodetector',
            icon: CKEDITOR.plugins.getPath('videodetector') + 'icons/icon_black.png'
        });

        CKEDITOR.dialog.add('videoDialog', this.path + 'dialogs/videoDialog.js');

        // Bind remove buttons.
        editor.on('contentDom', function(e) {
            var buttons = e.editor.document.find('input.remove-videodetector').toArray();
            for(var i = 0; i < buttons.length; i++) {
                buttons[i].on('click', function(e2) {
                    var to_remove = this.getParent();
                    if (to_remove.hasClass('videodetector')) {
                        to_remove.remove();
                    }
                })
            }
        });
    }
});